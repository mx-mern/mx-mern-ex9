$(document).ready(function () {
    var pageToken = 1;
    var nextPageToken = '';
    $('#result-list').empty();
    var startDate = new Date();
    var searchString ='';
    let oldKeyword='';
    let isLoading = false;


    $('#search').submit(function (event) {
        searchString = $('#keyword').val();
        //   debugger;
        $.ajax({
            method: 'get',
            url: `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q=${searchString}&type=video&key=AIzaSyA9gQZ-oYomFypZN7PsupZJtOfQqA6Q3qw`,
            success: function (data) {
                $('#result-list').empty();
                pageToken = data.nextPageToken;
                data.items.map(item => $('#result-list').append(`
            <div class="col-sm-1">
            <img src=${item.snippet.thumbnails.default.url}></img>
            <div>${item.snippet.title}</div>
            <a href="https://www.youtube.com/watch?v=${item.id.videoId}">https://www.youtube.com/watch?v=${item.id.videoId}</a>
            </div>
            `));
            }
        });
        //   debugger;
        event.preventDefault();
    })

    $(document).scroll(function (event) {
        // console.log('scrollTop()' + $(document).scrollTop() + 'document height: ' + $(document).height() +
        //     'window height: ' + $(window).height());
        // TODO:
        if ($(document).scrollTop() + $(window).height() == $(document).height()) {
            // console.log('Page is prepare to load more');
            if (!$('#result-list').is(':empty')) {
                //do something
                console.log('Page is continue to load more');
                $.ajax({
                    method: 'get',
                    url: `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q=${searchString}&type=video&key=AIzaSyA9gQZ-oYomFypZN7PsupZJtOfQqA6Q3qw&pageToken=${pageToken}`,
                    success: function (data) {
                        pageToken += data.nextPageToken;
                        data.items.map(item => $('#result-list').append(`
                    <div class="col-sm-1">
                    <img src=${item.snippet.thumbnails.default.url}></img>
                    <div>${item.snippet.title}</div>
                    <a href="https://www.youtube.com/watch?v=${item.id.videoId}">https://www.youtube.com/watch?v=${item.id.videoId}</a>
                    </div>
                    `));
                    }
                });
            }
        }
    });

    $('#keyword').on('input',function(e){
        function inputChangedTimeout() {
            if (oldKeyword != $('#keyword').val())
            {
                oldKeyword = $('#keyword').val();
                console.log("keyword changed: " + oldKeyword);
                searchString = oldKeyword;
                $('#isLoading').css('display', 'inline-block');
                $.ajax({
                    method: 'get',
                    url: `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q=${searchString}&type=video&key=AIzaSyA9gQZ-oYomFypZN7PsupZJtOfQqA6Q3qw`,
                    success: function (data) {
                        $('#result-list').empty();
                        pageToken = data.nextPageToken;
                        data.items.map(item => $('#result-list').append(`
                    <div class="col-sm-1">
                    <img src=${item.snippet.thumbnails.default.url}></img>
                    <div>${item.snippet.title}</div>
                    <a href="https://www.youtube.com/watch?v=${item.id.videoId}">https://www.youtube.com/watch?v=${item.id.videoId}</a>
                    </div>
                    `));
                    },
                    complete: function(jqXHR, textStatus )
                    {
                        $('#isLoading').css('display', 'none');
                    }
                });
            }
        }
        setTimeout(inputChangedTimeout, 2000);
    });

})